#ifndef EFTrecon_H
#define EFTrecon_H

#include "Analysis.h"

#include "EventBase.h"
#include "CutsEFTrecon.h"

#include <TTreeReader.h>
#include <string>

class EFTrecon: public Analysis {

public:
  EFTrecon(); 
  ~EFTrecon();

  void Initialize();
  void Execute();
  void Finalize();

protected:
  CutsEFTrecon* m_cutsEFTrecon;
  ConfigSvc* m_conf;

  ofstream outfile1;
  ofstream outfile2;
  ofstream outfile3;
  ofstream outfile4;
  ofstream outfile5;
  ofstream outfile6;
  ofstream outfile7;
  ofstream outfile8;
  ofstream outfile9;
  ofstream outfile10;
  ofstream outfile11;
  ofstream outfile12;
  ofstream outfile13;
  ofstream outfile14;
  ofstream outfile15;
  ofstream outfile16;
  
  int rawFileCount;
  string prevRawFileName;
};

#endif
