#ifndef CutsEFTrecon_H
#define CutsEFTrecon_H

#include "EventBase.h"

class CutsEFTrecon  {

public:
  CutsEFTrecon(EventBase* eventBase);
  ~CutsEFTrecon();
  bool EFTreconCutsOK();
  pair<vector<pair<int,float>>, vector<pair<int,float>>> GetAllSXs();

  static bool sortinrev(const pair<int,float> &a, const pair<int,float> &b){
    return (a.second > b.second);
  }

private:
  
  EventBase* m_event;

};

#endif
