#ifndef CutsEFTrecon_H
#define CutsEFTrecon_H

#include "EventBase.h"

class CutsEFTrecon  {

public:
  CutsEFTrecon(EventBase* eventBase);
  ~CutsEFTrecon();
  bool EFTreconCutsOK();


private:
  
  EventBase* m_event;

};

#endif
