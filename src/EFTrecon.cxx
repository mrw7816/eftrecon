#include "EventBase.h"
#include "Analysis.h"
#include "HistSvc.h"
#include "Logger.h"
#include "ConfigSvc.h"
#include "SkimSvc.h"
#include "SparseSvc.h"
#include "CutsBase.h"
#include "CutsEFTrecon.h"
#include "EFTrecon.h"

bool debug = false;

namespace {

  void Loggify(TAxis* axis) {
    int bins = axis->GetNbins();

    Axis_t from = log10(axis->GetXmin());
    Axis_t to = log10(axis->GetXmax());
    Axis_t width = (to - from) / bins;
    Axis_t *new_bins = new Axis_t[bins + 1];
  
    for (int i = 0; i <= bins; i++) new_bins[i] = pow(10, from + i * width);
    axis->Set(bins, new_bins); 
    delete[] new_bins; 
  }
  
  TH1* LoggifyX(TH1* h)  { Loggify(h->GetXaxis()); return h; }
  TH2* LoggifyXY(TH2* h) { Loggify(h->GetXaxis()); Loggify(h->GetYaxis()); return h;}
  TH2* LoggifyX(TH2* h)  { Loggify(h->GetXaxis()); return h; }
  TH2* LoggifyY(TH2* h)  { Loggify(h->GetYaxis()); return h; }
  
}

// Constructor
EFTrecon::EFTrecon()
  : Analysis()
{
  // Set up the expected event structure, include branches required for analysis.
  // List the branches required below, see full list here https://luxzeplin.gitlab.io/docs/softwaredocs/analysis/analysiswithrqs/rqlist.html
  // Load in the Single Scatters Branch
  m_event->IncludeBranch("ss");
  m_event->IncludeBranch("ms");
  m_event->IncludeBranch("pileUp");
  m_event->IncludeBranch("kr83m");
  m_event->IncludeBranch("other");
  m_event->IncludeBranch("pulsesTPC");
  m_event->Initialize();
  ////////

  // Setup logging
  logging::set_program_name("EFTrecon Analysis");
  
  // Setup the analysis specific cuts.
  m_cutsEFTrecon = new CutsEFTrecon(m_event);
  
  // Setup config service so it can be used to get config variables
  m_conf = ConfigSvc::Instance();
      
  //start the skimming service
  //m_skim = new SkimSvc();

  //set up the counting of raw files
  rawFileCount = 0;
}

// Destructor
EFTrecon::~EFTrecon() {
  delete m_cutsEFTrecon;
}

/////////////////////////////////////////////////////////////////
// Start of main analysis code block
// 
// Initialize() -  Called once before the event loop.
void EFTrecon::Initialize() {
  INFO("Initializing EFTrecon Analysis");
    
  //initialize mdc3 core cuts
  m_cuts->mdc3()->Initialize();

  //initialize skim files
  //m_skim->InitializeSkim(m_event); 
    /*
  outfile1.open("dev_ffn95_SS_100_500_Rn.txt");
  outfile2.open("dev_ffn95_MS_100_500_Rn.txt");
  outfile5.open("lzap_sparse_processing_scatterSelectionPerformance_Kr83m_outsideKrSize.txt");

  outfile8.open("lzap_sparse_processing_scatterSelectionPerformance_PileUp_HE.txt");

  outfile11.open("lzap_sparse_processing_scatterSelectionPerformance_Other_HE.txt");*/

  //raw file counting
  prevRawFileName= "test.root";

  //Histogram variables

  TH1* htmp = NULL; // ALPACA owns the booked histogram. We use temp pointers to manipulate the histograms.

  int nBinsS1 = 500;
  float minS1 = 7e-1;
  float maxS1 = 3e5;

  int nBinsS2 = 500;
  float minS2 = 1e2;
  float maxS2 = 5e7;

  int nBinsS1ratio = 500;
  float minS1ratio = 1e-6;
  float maxS1ratio = 1.1;

  int   nBinsS2ratio = 500;
  float minS2ratio   = 1e-6;
  float maxS2ratio   = 1.1;

  int   nBinsH2W = 500;
  float minH2W   = 1e-6;
  float maxH2W   = 10;

  int nBinsDrift = 500;
  float minDrift = 0;
  float maxDrift = 1000;

  int nBinsR2 = 500;
  float minR2 = 0;
  float maxR2 = 7000;

  int   nBinsXY = 500;
  float minXY   = -100;
  float maxXY   = 100;

  // All events
  htmp = m_hists->BookHist("all_s1_s2", nBinsS1, minS1, maxS1, nBinsS2, minS2, maxS2); LoggifyXY((TH2*)htmp);
  htmp = m_hists->BookHist("all_et_s1_s2", nBinsS1, minS1, maxS1, nBinsS2, minS2, maxS2); LoggifyXY((TH2*)htmp);
  htmp = m_hists->BookHist("all_net_s1_s2", nBinsS1, minS1, maxS1, nBinsS2, minS2, maxS2); LoggifyXY((TH2*)htmp);
  
  htmp = m_hists->BookHist("all_et_s1ratio_h2w_allS1s", nBinsS1ratio, minS1ratio, maxS1ratio, nBinsH2W, minH2W, maxH2W); LoggifyXY((TH2*)htmp);
  htmp = m_hists->BookHist("all_et_s1ratio_h2w_firstS1", nBinsS1ratio, minS1ratio, maxS1ratio, nBinsH2W, minH2W, maxH2W); LoggifyXY((TH2*)htmp);
  htmp = m_hists->BookHist("all_et_s2ratio_h2w_allS2s", nBinsS2ratio, minS2ratio, maxS2ratio, nBinsH2W, minH2W, maxH2W); LoggifyXY((TH2*)htmp);
  htmp = m_hists->BookHist("all_et_s2ratio_h2w_firstS2", nBinsS2ratio, minS2ratio, maxS2ratio, nBinsH2W, minH2W, maxH2W); LoggifyXY((TH2*)htmp);
  
  // SSs
  htmp = m_hists->BookHist("ss_s1_s2", nBinsS1, minS1, maxS1, nBinsS2, minS2, maxS2); LoggifyXY((TH2*)htmp); 
  htmp = m_hists->BookHist("ss_et_s1_s2", nBinsS1, minS1, maxS1, nBinsS2, minS2, maxS2); LoggifyXY((TH2*)htmp);
  htmp = m_hists->BookHist("ss_net_s1_s2", nBinsS1, minS1, maxS1, nBinsS2, minS2, maxS2); LoggifyXY((TH2*)htmp);
  htmp = m_hists->BookHist("ss_et_s1c_s2c", nBinsS1, minS1, maxS1, nBinsS2, minS2, maxS2); LoggifyXY((TH2*)htmp);
  htmp = m_hists->BookHist("ss_net_s1_s2_EFT", nBinsS1, minS1, maxS1, nBinsS2, minS2, maxS2); LoggifyXY((TH2*)htmp);
  htmp = m_hists->BookHist("ss_et_s1_s2_EFT", nBinsS1, minS1, maxS1, nBinsS2, minS2, maxS2); LoggifyXY((TH2*)htmp);

  htmp = m_hists->BookHist("ss_et_s1ratio_h2w", nBinsS1ratio, minS1ratio, maxS1ratio, nBinsH2W, minH2W, maxH2W); LoggifyXY((TH2*)htmp);
  htmp = m_hists->BookHist("ss_et_s2ratio_h2w", nBinsS2ratio, minS2ratio, maxS2ratio, nBinsH2W, minH2W, maxH2W); LoggifyXY((TH2*)htmp);

  
  // MSs
  htmp = m_hists->BookHist("ms_et_nS2", 25, 0, 25);

  htmp = m_hists->BookHist("ms_s1_s2sum", nBinsS1, minS1, maxS1, nBinsS2, minS2, maxS2); LoggifyXY((TH2*)htmp);
  htmp = m_hists->BookHist("ms_et_s1_s2sum", nBinsS1, minS1, maxS1, nBinsS2, minS2, maxS2); LoggifyXY((TH2*)htmp);
  htmp = m_hists->BookHist("ms_et_s1_s2sum_EFT", nBinsS1, minS1, maxS1, nBinsS2, minS2, maxS2); LoggifyXY((TH2*)htmp);
  htmp = m_hists->BookHist("ms_net_s1_s2sum_EFT", nBinsS1, minS1, maxS1, nBinsS2, minS2, maxS2); LoggifyXY((TH2*)htmp);
  htmp = m_hists->BookHist("ms_net_s1_s2sum", nBinsS1, minS1, maxS1, nBinsS2, minS2, maxS2); LoggifyXY((TH2*)htmp);
  htmp = m_hists->BookHist("ms_et_s1c_s2cSum", nBinsS1, minS1, maxS1, nBinsS2, minS2, maxS2); LoggifyXY((TH2*)htmp);

  htmp = m_hists->BookHist("ms_et_s1ratio_h2w", nBinsS1ratio, minS1ratio, maxS1ratio, nBinsH2W, minH2W, maxH2W); LoggifyXY((TH2*)htmp);
  htmp = m_hists->BookHist("ms_et_s2ratio_h2w", nBinsS2ratio, minS2ratio, maxS2ratio, nBinsH2W, minH2W, maxH2W); LoggifyXY((TH2*)htmp);
  
  // Kr83m
  htmp = m_hists->BookHist("kr_s1a_s2", nBinsS1, minS1, maxS1, nBinsS2, minS2, maxS2); LoggifyXY((TH2*)htmp);
  htmp = m_hists->BookHist("kr_et_s1a_s2", nBinsS1, minS1, maxS1, nBinsS2, minS2, maxS2); LoggifyXY((TH2*)htmp);
  htmp = m_hists->BookHist("kr_et_s1b_s2", nBinsS1, minS1, maxS1, nBinsS2, minS2, maxS2); LoggifyXY((TH2*)htmp);
  htmp = m_hists->BookHist("kr_et_s1tot_s2", nBinsS1, minS1, maxS1, nBinsS2, minS2, maxS2); LoggifyXY((TH2*)htmp);
  htmp = m_hists->BookHist("kr_et_s1cTot_s2c", nBinsS1, minS1, maxS1, nBinsS2, minS2, maxS2); LoggifyXY((TH2*)htmp);
  htmp = m_hists->BookHist("kr_net_s1a_s2", nBinsS1, minS1, maxS1, nBinsS2, minS2, maxS2); LoggifyXY((TH2*)htmp);
  htmp = m_hists->BookHist("kr_et_s1a_s2_EFT", nBinsS1, minS1, maxS1, nBinsS2, minS2, maxS2); LoggifyXY((TH2*)htmp);

    
  htmp = m_hists->BookHist("kr_et_s1ratio_h2w", nBinsS1ratio, minS1ratio, maxS1ratio, nBinsH2W, minH2W, maxH2W); LoggifyXY((TH2*)htmp);
  htmp = m_hists->BookHist("kr_et_s2ratio_h2w", nBinsS2ratio, minS2ratio, maxS2ratio, nBinsH2W, minH2W, maxH2W); LoggifyXY((TH2*)htmp);
  
  htmp = m_hists->BookHist("kr_et_timeDist", 2500, 0, 25000);
  
  // Pile up
  htmp = m_hists->BookHist("pu_et_nS1", 25, 0, 25);
  htmp = m_hists->BookHist("pu_et_nS2", 25, 0, 25);

  htmp = m_hists->BookHist("pu_s1_s2", nBinsS1, minS1, maxS1, nBinsS2, minS2, maxS2); LoggifyXY((TH2*)htmp);
  htmp = m_hists->BookHist("pu_et_s1_s2", nBinsS1, minS1, maxS1, nBinsS2, minS2, maxS2); LoggifyXY((TH2*)htmp);
  htmp = m_hists->BookHist("pu_net_s1_s2", nBinsS1, minS1, maxS1, nBinsS2, minS2, maxS2); LoggifyXY((TH2*)htmp);

  htmp = m_hists->BookHist("pu_et_s1ratio_h2w", nBinsS1ratio, minS1ratio, maxS1ratio, nBinsH2W, minH2W, maxH2W); LoggifyXY((TH2*)htmp);
  htmp = m_hists->BookHist("pu_et_s2ratio_h2w", nBinsS2ratio, minS2ratio, maxS2ratio, nBinsH2W, minH2W, maxH2W); LoggifyXY((TH2*)htmp);

  htmp = m_hists->BookHist("pu_et_timeDist", 2500, 0, 25000);

  // Other
  htmp = m_hists->BookHist("other_s1_s2", nBinsS1, minS1, maxS1, nBinsS2, minS2, maxS2); LoggifyXY((TH2*)htmp);
  htmp = m_hists->BookHist("other_et_s1_s2", nBinsS1, minS1, maxS1, nBinsS2, minS2, maxS2); LoggifyXY((TH2*)htmp);
  htmp = m_hists->BookHist("other_net_s1_s2", nBinsS1, minS1, maxS1, nBinsS2, minS2, maxS2); LoggifyXY((TH2*)htmp);

  // Event type histos
  m_hists->BookHist("eventType", 6, 0, 6);
  m_hists->BookHist("eventType_et", 6, 0, 6);
  m_hists->BookHist("otherType", 5, 0, 5);
  m_hists->BookHist("otherType_et", 5, 0, 5);

  if (debug) cout << "Finished Initialize()" << endl;
    
}


// Execute() - Called once per event.
void EFTrecon::Execute() {

  if (debug) cout << "Started Execute()" << endl;
  
  //Etrain veto things
  m_cuts->mdc3()->InitializeETrainVeto();

  // livetime things
  int eventID = (*m_event->m_eventHeader)->eventID;
  int runID =(*m_event->m_eventHeader)->runID;
  string rawFileName = (*m_event->m_eventHeader)->rawFileName;
  string rawFilePath = "/global/cfs/cdirs/lz/data/MDC3/calibration/BACCARAT-4.10.3_DER-8.5.13/20180312/"; // For commissioning data. Kr83m.

  if (rawFileName.compare(prevRawFileName) != 0) {
    rawFileCount++;
    //cout << "Raw file count: " << rawFileCount << endl;
    prevRawFileName = rawFileName;
  }

  // Get the max and sub S1s and S2s
  pair<vector<pair<int,float>>, vector<pair<int,float>>> allSXs = m_cutsEFTrecon->GetAllSXs(); // goal is to run through all pulses only once
  vector<pair<int,float>> allS1s = allSXs.first;
  vector<pair<int,float>> allS2s = allSXs.second;
  int   maxS1pulseID = allS1s[0].first;
  float maxS1area    = allS1s[0].second;
  int   maxS2pulseID = allS2s[0].first;
  float maxS2area    = allS2s[0].second;

  if (debug) cout << "Got RQs" << endl;
  
  // evaluate ETrain cut only once
  bool S1ETrainVeto = false;
  if (maxS1pulseID > -1) S1ETrainVeto = m_cuts->mdc3()->ETrainVeto(maxS1pulseID);
  else if (maxS1pulseID == -1 && (*m_event->m_tpcPulses)->nPulses != 0) S1ETrainVeto = m_cuts->mdc3()->ETrainVeto(0);
  else S1ETrainVeto = true;

  if (debug) cout << "Did ET veto" << endl;
  
  // Make the all events histograms
  if (maxS1pulseID > -1 && maxS2pulseID > -1) { // make sure there is is least 1 S1 and 1 S2 in the event

    m_hists->GetHistFromMap("all_s1_s2")->Fill(maxS1area, maxS2area);
    if (S1ETrainVeto) m_hists->GetHistFromMap("all_net_s1_s2")->Fill(maxS1area, maxS2area);
    if (!S1ETrainVeto) {
	
      m_hists->GetHistFromMap("all_et_s1_s2")->Fill(maxS1area, maxS2area);

      string date = rawFileName.substr(rawFileName.find_first_of("_")+1,rawFileName.find_first_of("_")+6);
      if (allS1s.size() > 1) { //make sure there are second pulses to compare against...
        float H2WS1 = (*m_event->m_tpcPulses)->peakAmp[allS1s[1].first] / ( (*m_event->m_tpcPulses)->areaFractionTime95_ns[allS1s[1].first] - (*m_event->m_tpcPulses)->areaFractionTime5_ns[allS1s[1].first] );
        float S1ratio = allS1s[1].second / maxS1area;
        m_hists->GetHistFromMap("all_et_s1ratio_h2w_firstS1")->Fill(S1ratio, H2WS1);

        for (int n=1; n<allS1s.size(); n++) {
          H2WS1 = (*m_event->m_tpcPulses)->peakAmp[allS1s[n].first] / ( (*m_event->m_tpcPulses)->areaFractionTime95_ns[allS1s[n].first] - (*m_event->m_tpcPulses)->areaFractionTime5_ns[allS1s[n].first] );
          S1ratio = allS1s[n].second / maxS1area;
          m_hists->GetHistFromMap("all_et_s1ratio_h2w_allS1s")->Fill(S1ratio, H2WS1);
	      }
      } // end S1s

      if (allS2s.size() > 1) {
        float H2WS2 = (*m_event->m_tpcPulses)->peakAmp[allS2s[1].first] / ( (*m_event->m_tpcPulses)->areaFractionTime95_ns[allS2s[1].first] - (*m_event->m_tpcPulses)->areaFractionTime5_ns[allS2s[1].first] );
        float S2ratio = allS2s[1].second / maxS2area;
        m_hists->GetHistFromMap("all_et_s2ratio_h2w_firstS2")->Fill(S2ratio, H2WS2);

        for (int m=1; m<allS2s.size(); m++) {
          H2WS2 = (*m_event->m_tpcPulses)->peakAmp[allS2s[m].first] / ( (*m_event->m_tpcPulses)->areaFractionTime95_ns[allS2s[m].first] - (*m_event->m_tpcPulses)->areaFractionTime5_ns[allS2s[m].first] );
          S2ratio = allS2s[m].second / maxS2area;
          m_hists->GetHistFromMap("all_et_s2ratio_h2w_allS2s")->Fill(S2ratio, H2WS2);
        }
      }// end S2s
    } // etrain veto
  } // end all events

  // What type of scatter is it?
  int numSS = (*m_event->m_singleScatter)->nSingleScatters;
  int numMS = (*m_event->m_multipleScatter)->nMultipleScatters;
  int numKr = (*m_event->m_kr83mScatter)->nKr83mScatters;
  int numPU = (*m_event->m_pileupScatter)->nPileUpScatters;
  int numOS = (*m_event->m_otherScatter)->nOtherScatters;
  
  // if there is a single scatter in the event then plot the S1 pulse area
  if (numSS > 0){

    if (debug) cout << "Started SS" << endl;
    
    m_hists->GetHistFromMap("eventType")->Fill(0);
    if (!S1ETrainVeto) m_hists->GetHistFromMap("eventType_et")->Fill(0);
    
    float s1area = (*m_event->m_singleScatter)->s1Area_phd;
    float s2area = (*m_event->m_singleScatter)->s2Area_phd;
    float s1c = (*m_event->m_singleScatter)->correctedS1Area_phd;
    float s2c = (*m_event->m_singleScatter)->correctedS2Area_phd;

    m_hists->GetHistFromMap("ss_s1_s2")->Fill(s1area, s2area);
    if (!S1ETrainVeto) m_hists->GetHistFromMap("ss_et_s1_s2")->Fill(s1area, s2area);
    if (S1ETrainVeto) m_hists->GetHistFromMap("ss_net_s1_s2")->Fill(s1area, s2area);
    if (!S1ETrainVeto) m_hists->GetHistFromMap("ss_et_s1c_s2c")->Fill(s1c, s2c);
      
    if ((s1area > 100) && (s1area < 500) ) { 
        if ((s2area > 420) && (s2area < 1e8)){
            if (!S1ETrainVeto) m_hists->GetHistFromMap("ss_et_s1_s2_EFT")->Fill(s1area, s2area);
            if (S1ETrainVeto) m_hists->GetHistFromMap("ss_net_s1_s2_EFT")->Fill(s1area, s2area);
           /* string date = rawFileName.substr(rawFileName.find_first_of("_")+1,rawFileName.find_first_of("_")+6);
            if(!gSystem->AccessPathName(Form("%s/%s/%s",rawFilePath.c_str(),date.c_str(),rawFileName.c_str()))) {
              outfile1 << Form("%s/%s/%s",rawFilePath.c_str(),date.c_str(),rawFileName.c_str()) << "," << runID << "," << eventID  <<endl;
            }*/
        }
      }
      
    if (!S1ETrainVeto) {

      if (allS1s.size() > 1) {
	float h2wS1 = (*m_event->m_tpcPulses)->peakAmp[allS1s[1].first] / ( (*m_event->m_tpcPulses)->areaFractionTime95_ns[allS1s[1].first] - (*m_event->m_tpcPulses)->areaFractionTime5_ns[allS1s[1].first] );
	float s1ratio = allS1s[1].second / s1area;
	m_hists->GetHistFromMap("ss_et_s1ratio_h2w")->Fill(s1ratio,h2wS1);
      }

      if (allS2s.size() > 1) {
	float h2wS2 = (*m_event->m_tpcPulses)->peakAmp[allS2s[1].first] / ( (*m_event->m_tpcPulses)->areaFractionTime95_ns[allS2s[1].first] - (*m_event->m_tpcPulses)->areaFractionTime5_ns[allS2s[1].first] );
	float s2ratio = allS2s[1].second / s2area;
	m_hists->GetHistFromMap("ss_et_s2ratio_h2w")->Fill(s2ratio,h2wS2);
      }
     
    } //end st veto
  } // end SS

  if (numMS > 0) {

    if (debug) cout << "Started MS" << endl;

    m_hists->GetHistFromMap("eventType")->Fill(1);
    if (!S1ETrainVeto) m_hists->GetHistFromMap("eventType_et")->Fill(1);

    float s1area = (*m_event->m_multipleScatter)->s1Area_phd;
    vector<float> s1c = (*m_event->m_multipleScatter)->correctedS1Areas_phd;
    
    int nS2s = (*m_event->m_multipleScatter)->nS2s;    
    vector<int> s2pulseIDs = (*m_event->m_multipleScatter)->s2PulseIDs;
    vector<pair<int, float>> s2pulses;
    for (int n=0; n<nS2s; n++) {
      s2pulses.push_back(make_pair(s2pulseIDs[n], (*m_event->m_tpcPulses)->pulseArea_phd[s2pulseIDs[n]]));
    }
    sort(s2pulses.begin(), s2pulses.end(), CutsEFTrecon::sortinrev);

    vector<float> s2c = (*m_event->m_multipleScatter)->correctedS2Area_phd;
    float s2sum = 0;
    float s2cSum = 0;
    for (int n=0; n<nS2s; n++) {
      s2sum += s2pulses[n].second;
      s2cSum += s2c[n];
    }

    m_hists->GetHistFromMap("ms_s1_s2sum")->Fill(s1area, s2sum);
    if (!S1ETrainVeto) {
      m_hists->GetHistFromMap("ms_et_s1_s2sum")->Fill(s1area, s2sum);
    }
    if (S1ETrainVeto) m_hists->GetHistFromMap("ms_net_s1_s2sum")->Fill(s1area, s2sum);
    if (!S1ETrainVeto) m_hists->GetHistFromMap("ms_et_s1c_s2cSum")->Fill(s1c[s2pulses[0].first], s2cSum);
      
    if ((s1area > 100) && (s1area < 500)) { 
        if((s2sum > 420) && (s2sum < 1e8)){
            if (!S1ETrainVeto) m_hists->GetHistFromMap("ms_et_s1_s2_EFT")->Fill(s1area, s2sum);
            if (S1ETrainVeto) m_hists->GetHistFromMap("ms_net_s1_s2_EFT")->Fill(s1area, s2sum);
           /* 
            string date = rawFileName.substr(rawFileName.find_first_of("_")+1,rawFileName.find_first_of("_")+6);
            if(!gSystem->AccessPathName(Form("%s/%s/%s",rawFilePath.c_str(),date.c_str(),rawFileName.c_str()))) {
              outfile2 << Form("%s/%s/%s",rawFilePath.c_str(),date.c_str(),rawFileName.c_str()) << "," << runID << "," << eventID  <<endl;
            }*/
        }
      }    

    if (!S1ETrainVeto) {

      m_hists->GetHistFromMap("ms_et_nS2")->Fill(nS2s);

      if (allS1s.size() > 1) {
        float h2wS1 = (*m_event->m_tpcPulses)->peakAmp[allS1s[1].first] / ( (*m_event->m_tpcPulses)->areaFractionTime95_ns[allS1s[1].first] - (*m_event->m_tpcPulses)->areaFractionTime5_ns[allS1s[1].first] );
        float s1ratio = allS1s[1].second / s1area;
        m_hists->GetHistFromMap("ms_et_s1ratio_h2w")->Fill(s1ratio,h2wS1);
      }
      
      for (int n=0; n<nS2s; n++) {

        if (n>0) { // skip this for the main S2
          float h2wS2 = (*m_event->m_tpcPulses)->peakAmp[s2pulses[n].first] / ( (*m_event->m_tpcPulses)->areaFractionTime95_ns[s2pulses[n].first] - (*m_event->m_tpcPulses)->areaFractionTime5_ns[s2pulses[n].first] );
          float s2ratio = s2pulses[n].second / s2pulses[0].second;
          m_hists->GetHistFromMap("ms_et_s2ratio_h2w")->Fill(s2ratio,h2wS2);
        }

      } // end S2s loop
    } //end st veto
  } // end MS

  if (numKr > 0) {

    if (debug) cout << "Started Kr" << endl;
    
    m_hists->GetHistFromMap("eventType")->Fill(2);
    if (!S1ETrainVeto) m_hists->GetHistFromMap("eventType_et")->Fill(2);

    float s1a = (*m_event->m_kr83mScatter)->s1aArea_phd;
    float s1b = (*m_event->m_kr83mScatter)->s1bArea_phd;
    float s1c = (*m_event->m_kr83mScatter)->correctedS1aArea_phd + (*m_event->m_kr83mScatter)->correctedS1bArea_phd;
    float s2area = (*m_event->m_kr83mScatter)->s2Area_phd;
    float s2c = (*m_event->m_kr83mScatter)->correctedS2Area_phd;

    m_hists->GetHistFromMap("kr_s1a_s2")->Fill(s1a, s2area);
    if (!S1ETrainVeto) {
      m_hists->GetHistFromMap("kr_et_s1a_s2")->Fill(s1a, s2area);
      m_hists->GetHistFromMap("kr_et_s1b_s2")->Fill(s1b, s2area);
      m_hists->GetHistFromMap("kr_et_s1tot_s2")->Fill(s1a+s1b, s2area);
      m_hists->GetHistFromMap("kr_et_s1cTot_s2c")->Fill(s1c, s2c);
    }
    if (S1ETrainVeto) m_hists->GetHistFromMap("kr_net_s1a_s2")->Fill(s1a, s2area);

    if (!S1ETrainVeto) {

      int biggerS1 = -1;
      int smallerS1 = -1;
      if (s1a>s1b){
	biggerS1 = (*m_event->m_kr83mScatter)->s1aPulseID;
	smallerS1 = (*m_event->m_kr83mScatter)->s1bPulseID;
      }
      else {
	biggerS1 = (*m_event->m_kr83mScatter)->s1bPulseID;
	smallerS1 = (*m_event->m_kr83mScatter)->s1aPulseID;
      }
      float h2wS1 = (*m_event->m_tpcPulses)->peakAmp[smallerS1] / ( (*m_event->m_tpcPulses)->areaFractionTime95_ns[smallerS1] - (*m_event->m_tpcPulses)->areaFractionTime5_ns[smallerS1] );
      float s1ratio = (*m_event->m_tpcPulses)->pulseArea_phd[smallerS1] / (*m_event->m_tpcPulses)->pulseArea_phd[biggerS1];
      m_hists->GetHistFromMap("kr_et_s1ratio_h2w")->Fill(s1ratio,h2wS1);

      if (allS2s.size() > 1) {
        float h2wS2 = (*m_event->m_tpcPulses)->peakAmp[allS2s[1].first] / ( (*m_event->m_tpcPulses)->areaFractionTime95_ns[allS2s[1].first] - (*m_event->m_tpcPulses)->areaFractionTime5_ns[allS2s[1].first] );
        float s2ratio = allS2s[1].second / s2area;
        m_hists->GetHistFromMap("kr_et_s2ratio_h2w")->Fill(s2ratio,h2wS2);
      }
      

      m_hists->GetHistFromMap("kr_et_timeDist")->Fill( abs( (*m_event->m_tpcPulses)->pulseStartTime_ns[(*m_event->m_kr83mScatter)->s1aPulseID] - (*m_event->m_tpcPulses)->pulseStartTime_ns[(*m_event->m_kr83mScatter)->s1bPulseID] ) );
      
      string date = rawFileName.substr(rawFileName.find_first_of("_")+1,rawFileName.find_first_of("_")+6);
      if (s1a > 100 && s1a < 500) {
          if (s2area > 420 && s2area < 1e8) {
            m_hists->GetHistFromMap("kr_et_s1a_s2_EFT")->Fill(s1a, s2area);
           /* string date = rawFileName.substr(rawFileName.find_first_of("_")+1,rawFileName.find_first_of("_")+6);
            if(!gSystem->AccessPathName(Form("%s/%s/%s",rawFilePath.c_str(),date.c_str(),rawFileName.c_str()))) {
              outfile5 << Form("%s/%s/%s",rawFilePath.c_str(),date.c_str(),rawFileName.c_str()) << "," << runID << "," << eventID  <<endl;
        }*/
       }
      }
    } //end et veto
  } // end Kr

  if (numPU > 0) {

    if (debug) cout << "Started PU" << endl;

    m_hists->GetHistFromMap("eventType")->Fill(3);
    if (!S1ETrainVeto) m_hists->GetHistFromMap("eventType_et")->Fill(3);
    
    int nS1s = (*m_event->m_pileupScatter)->nS1;
    int nS2s = (*m_event->m_pileupScatter)->nS2;
    
    // Get and sort S1 and S2 areas and pulse IDs
    vector<int> s1pulseIDs = (*m_event->m_pileupScatter)->s1PulseIDs;
    vector<pair<int, float>> s1pulses;
    for (int n=0; n<s1pulseIDs.size(); n++) {
      s1pulses.push_back(make_pair(s1pulseIDs[n], (*m_event->m_tpcPulses)->pulseArea_phd[s1pulseIDs[n]]));
    }
    sort(s1pulses.begin(), s1pulses.end(), CutsEFTrecon::sortinrev);
    
    vector<int> s2pulseIDs = (*m_event->m_pileupScatter)->s2PulseIDs;
    vector<pair<int, float>> s2pulses;
    for (int n=0; n<s2pulseIDs.size(); n++) {
      s2pulses.push_back(make_pair(s2pulseIDs[n], (*m_event->m_tpcPulses)->pulseArea_phd[s2pulseIDs[n]]));
    }
    sort(s2pulses.begin(), s2pulses.end(), CutsEFTrecon::sortinrev);

    
    m_hists->GetHistFromMap("pu_s1_s2")->Fill(s1pulses[0].second, s2pulses[0].second);
    if (!S1ETrainVeto) m_hists->GetHistFromMap("pu_et_s1_s2")->Fill(s1pulses[0].second, s2pulses[0].second);
    if (S1ETrainVeto) m_hists->GetHistFromMap("pu_net_s1_s2")->Fill(s1pulses[0].second, s2pulses[0].second);

    if (!S1ETrainVeto) {

      m_hists->GetHistFromMap("pu_et_nS1")->Fill(nS1s);
      m_hists->GetHistFromMap("pu_et_nS2")->Fill(nS2s);

      string date = rawFileName.substr(rawFileName.find_first_of("_")+1,rawFileName.find_first_of("_")+6);
      if (s1pulses[0].second > 400) {
        /*string date = rawFileName.substr(rawFileName.find_first_of("_")+1,rawFileName.find_first_of("_")+6);
        if(!gSystem->AccessPathName(Form("%s/%s/%s",rawFilePath.c_str(),date.c_str(),rawFileName.c_str()))) {
          outfile8 << Form("%s/%s/%s",rawFilePath.c_str(),date.c_str(),rawFileName.c_str()) << "," << runID << "," << eventID  <<endl;
        }*/
      }

      for (int n=1; n<nS1s; n++) {
	
	float h2wS1 = (*m_event->m_tpcPulses)->peakAmp[s1pulses[n].first] / ( (*m_event->m_tpcPulses)->areaFractionTime95_ns[s1pulses[n].first] - (*m_event->m_tpcPulses)->areaFractionTime5_ns[s1pulses[n].first] );
	float s1ratio = (*m_event->m_tpcPulses)->pulseArea_phd[s1pulses[n].first] / (*m_event->m_tpcPulses)->pulseArea_phd[s1pulses[0].first] ;
	m_hists->GetHistFromMap("pu_et_s1ratio_h2w")->Fill(s1ratio,h2wS1);

      } // end S1s loop

      for (int n=1; n<nS2s; n++) {

	float h2wS2 = (*m_event->m_tpcPulses)->peakAmp[s2pulses[n].first] / ( (*m_event->m_tpcPulses)->areaFractionTime95_ns[s2pulses[n].first] - (*m_event->m_tpcPulses)->areaFractionTime5_ns[s2pulses[n].first] );
	float s2ratio = (*m_event->m_tpcPulses)->pulseArea_phd[s2pulses[n].first] / (*m_event->m_tpcPulses)->pulseArea_phd[s2pulses[0].first];
	m_hists->GetHistFromMap("pu_et_s2ratio_h2w")->Fill(s2ratio,h2wS2);
	
      } // end S2s loop

      m_hists->GetHistFromMap("pu_et_timeDist")->Fill( abs( (*m_event->m_tpcPulses)->pulseStartTime_ns[s1pulses[0].first] - (*m_event->m_tpcPulses)->pulseStartTime_ns[s1pulses[1].first] ) );
      
    } // end et veto
  } // end PU

  if (numOS > 0) {

    if (debug) cout << "Started Other" << endl;
    
    m_hists->GetHistFromMap("eventType")->Fill(4);
    if (!S1ETrainVeto) m_hists->GetHistFromMap("eventType_et")->Fill(4);

    if ( (*m_event->m_otherScatter)->nS1s != 0 && (*m_event->m_otherScatter)->nS2s != 0 ) m_hists->GetHistFromMap("otherType")->Fill(0);
    if ( (*m_event->m_otherScatter)->nS1s == 0 && (*m_event->m_otherScatter)->nS2s != 0 ) m_hists->GetHistFromMap("otherType")->Fill(1);
    if ( (*m_event->m_otherScatter)->nS1s != 0 && (*m_event->m_otherScatter)->nS2s == 0 ) m_hists->GetHistFromMap("otherType")->Fill(2);
    if ( (*m_event->m_otherScatter)->nS1s == 0 && (*m_event->m_otherScatter)->nS2s == 0 && (*m_event->m_tpcPulses)->nPulses != 0 ) m_hists->GetHistFromMap("otherType")->Fill(3);
    if ( (*m_event->m_otherScatter)->nS1s == 0 && (*m_event->m_otherScatter)->nS2s == 0 && (*m_event->m_tpcPulses)->nPulses == 0 ) m_hists->GetHistFromMap("otherType")->Fill(4);

    if (!S1ETrainVeto) {
      if ( (*m_event->m_otherScatter)->nS1s != 0 && (*m_event->m_otherScatter)->nS2s != 0 ) m_hists->GetHistFromMap("otherType_et")->Fill(0);
      if ( (*m_event->m_otherScatter)->nS1s == 0 && (*m_event->m_otherScatter)->nS2s != 0 ) m_hists->GetHistFromMap("otherType_et")->Fill(1);
      if ( (*m_event->m_otherScatter)->nS1s != 0 && (*m_event->m_otherScatter)->nS2s == 0 ) m_hists->GetHistFromMap("otherType_et")->Fill(2);
      if ( (*m_event->m_otherScatter)->nS1s == 0 && (*m_event->m_otherScatter)->nS2s == 0 && (*m_event->m_tpcPulses)->nPulses != 0 ) m_hists->GetHistFromMap("otherType_et")->Fill(3);
      if ( (*m_event->m_otherScatter)->nS1s == 0 && (*m_event->m_otherScatter)->nS2s == 0 && (*m_event->m_tpcPulses)->nPulses == 0 ) m_hists->GetHistFromMap("otherType_et")->Fill(4);
    }
    
    m_hists->GetHistFromMap("other_s1_s2")->Fill(maxS1area, maxS2area);
    if (!S1ETrainVeto) {
      
      m_hists->GetHistFromMap("other_et_s1_s2")->Fill(maxS1area, maxS2area);
/*
      string date = rawFileName.substr(rawFileName.find_first_of("_")+1,rawFileName.find_first_of("_")+6);
      if (maxS1area > 400) {
        if(!gSystem->AccessPathName(Form("%s/%s/%s",rawFilePath.c_str(),date.c_str(),rawFileName.c_str()))) {
          outfile11 << Form("%s/%s/%s",rawFilePath.c_str(),date.c_str(),rawFileName.c_str()) << "," << runID << "," << eventID  <<endl;
        }
      }*/
    }

    if (S1ETrainVeto) m_hists->GetHistFromMap("other_net_s1_s2")->Fill(maxS1area, maxS2area);
  } // end other

  // Fill event type histograms for events with no pulses
  if ((*m_event->m_tpcPulses)->nPulses == 0) {
    m_hists->GetHistFromMap("eventType")->Fill(5);
    if (!S1ETrainVeto) m_hists->GetHistFromMap("eventType_et")->Fill(5);
  }

  if (debug) cout << "Finished scatters - doing WS ROI" << endl;

  
  if (debug) cout << "Finished Execute()" << endl;
}

// Finalize() - Called once after event loop.
void EFTrecon::Finalize() {
  INFO("Finalizing EFTrecon Analysis");
  //cout << "TOTAL LIVETIME FROM RAW FILES: " << rawFileCount*4 << " seconds" << endl; 
 // m_hists->BookFillHist("rawFiles",25000,0,25000,rawFileCount,rawFileCount);
  
  //delete m_skim;
  /*
  outfile5.close();
  outfile6.close();
  outfile7.close();
  outfile8.close();
  outfile9.close();
  outfile10.close();
  outfile11.close();
  outfile16.close();*/
}

